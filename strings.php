<?php namespace ironwoods\tools\strings;

/**
 * @file: strings.php
 * @info: Main file for the librarie "Strings"
 *
 *
 * @author: Moisés Alcocer
 * 2018, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.strings
 */

require 'classes/stringarrays.php';
require 'classes/stringchecks.php';
require 'classes/stringtools.php';
require 'classes/stringtransformer.php';

use ironwoods\tools\strings\classes\StringArrays as StringArrays;
use ironwoods\tools\strings\classes\StringChecks as StringChecks;
use ironwoods\tools\strings\classes\StringTools as StringTools;
use ironwoods\tools\strings\classes\StringTransformer as StringTransformer;


final class Strings
{

    /******************************************************************/
    /*** Properties declaration ***************************************/


    /******************************************************************/
    /*** Methods declaration  *****************************************/

    /*** Public Methods ***********************************************/

        /**
         * Cuts a multi-row string into rows
         *
         * @param  string       $str
         * @return array
         */
        public static function getRows(string $str): array
        {
            return StringTools::getRows($str);
        }

        /**
         * Gets the strings with the fragment inside
         *
         * @param  array        $strs
         * @param  string       $needle
         * @return array
         */
        public static function getWith(array $strs, string $needle): array
        {
            return StringArrays::getWith($strs, $needle);
        }

        /**
         * Searchs for special chars
         *
         * @param  string       $str
         * @return boolean
         */
        public static function hasSpecialChars(string $str): bool
        {
            return StringChecks::hasSpecialChars($str);
        }

        /**
         * Searchs for a string content
         *
         * @param  string       $str
         * @param  string       $needle
         * @return boolean
         */
        public static function isIn(string $str, string $needle): bool
        {
            return StringChecks::isIn($str, $needle);
        }

        /**
         * Searchs a string inside the array
         *
         * @param  array        $strs
         * @param  string       $needle
         * @return boolean
         */
        public static function isInArray(array $strs, string $needle): bool
        {
            return StringArrays::content($strs, $needle);
        }

        /**
         * Replaces slashes
         *
         * @param  string       $path
         * @return string
         */
        public static function replaceSlashes(string $path): string
        {
            return StringTransformer::replaceSlashes($path);
        }

        /**
         * Replaces special chars
         *
         * @param  string       $str
         * @return string
         */
        public static function replaceSpecialChars(string $str): string
        {
            return StringTransformer::replaceSpecialChars($str);
        }

        public static function validateChars(
            string $str,
            string $validChars
        ): bool
        {
            return StringChecks::validateChars($str, $validChars);
        }

        public static function validateLength(
            string $str,
            int $minLength,
            int $maxLength
        ): bool
        {
            return StringChecks::validateLength($str, $minLength, $maxLength);
        }


} //class
