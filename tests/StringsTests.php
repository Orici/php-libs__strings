<?php namespace ironwoods\tools\Strings\tests;
/*
cd c:/xampp/htdocs/desarrollos/libraries/_strings/tests
phpunit StringsTests.php --colors=always --repeat 10
*/

$_BASE_PATH = dirname( __FILE__, 2 ) . '/'; //Only PHP 7
require $_BASE_PATH . 'strings.php';

use \PHPUnit\Framework\TestCase;
use ironwoods\tools\strings\Strings as Strings;


class StringsTests extends TestCase
{

    /**
     * @coversDefaultClass \ironwoods\tools\strings\Strings
     * @covers Strings::getRows
     */
    public function testGetRows()
    {
        $str = "Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Quis eum, illo corporis nemo repudiandae sed libero,
            at similique mollitia totam eius sint incidunt debitis.";
        $arr_pairs = Strings::getRows($str);

        self::assertTrue(is_array($arr_pairs));
        self::assertTrue(count($arr_pairs) === 3);
    }

    /**
     * @coversDefaultClass \ironwoods\tools\strings\Strings
     * @covers Strings::getWith
     */
    public function testGetWith()
    {
        $origin = ['apple', 'lemon', 'pear', 'pineapple', 'orange'];
        $needle = 'pp';

        $expResult = ['apple', 'pineapple'];
        $result    = Strings::getWith($origin, $needle);
        self::assertEquals($expResult, $result);
    }

    /**
     * @coversDefaultClass \ironwoods\tools\strings\Strings
     * @covers Strings::hasSpecialChars
     */
    public function testHasSpecialChars()
    {
        self::assertTrue(Strings::hasSpecialChars("Oñón"));
        self::assertFalse(Strings::hasSpecialChars("hola"));
    }

    /**
     * @coversDefaultClass \ironwoods\tools\strings\Strings
     * @covers Strings::isIn
     */
    public function testIsIn()
    {
        self::assertTrue(Strings::isIn("hola", "ho"));
        self::assertTrue(Strings::isIn("hola", "a"));
        self::assertFalse(Strings::isIn("hola", "x"));
    }

    /**
     * @coversDefaultClass \ironwoods\tools\strings\Strings
     * @covers Strings::isInArray
     */
    public function testIsInArray()
    {
        $origin = ['apple', 'lemon', 'pear', 'pineapple', 'orange'];

        self::assertTrue(Strings::isInArray($origin, 'lemon'));
        self::assertFalse(Strings::isInArray($origin, 'grape'));
    }

    /**
     * @coversDefaultClass \ironwoods\tools\strings\Strings
     * @covers Strings::replaceSlashes
     */
    public function testReplaceSlashes()
    {
        $origin    = "c:\\kla/oues:\kerón";
        $expResult = "c:/kla/oues:/kerón";
        $result    = Strings::replaceSlashes($origin);

        self::assertEquals($expResult, $result);
    }

    /**
     * @coversDefaultClass \ironwoods\tools\strings\Strings
     * @covers Strings::replaceSpecialChars
     */
    public function testReplaceSpecialChars()
    {
        $origin    = "kerón, Pingüi & Ñu";
        $expResult = "keron, Pingui & Nu";
        $result    = Strings::replaceSpecialChars($origin);

        self::assertEquals($expResult, $result);
    }

    public function testValidateChars()
    {
        $validChars = 'abcABCáÁ';
        $origin     = "aAáÁ";

        $result = Strings::validateChars($origin, $validChars);
        self::assertTrue($result);

        ////////////////////////////////////////////////////////////////
        $origin     = "aeiou";

        $result = Strings::validateChars($origin, $validChars);
        self::assertFalse($result);
    }

    public function testValidateLength()
    {
        $origin    = "kerón, Pingüi & Ñu"; // 18 chars //

        self::assertTrue(Strings::validateLength($origin, 15, 20));
        self::assertTrue(Strings::validateLength($origin, 18, 20));
        self::assertTrue(Strings::validateLength($origin, 18, 18));
        self::assertTrue(Strings::validateLength($origin, 15, 18));

        self::assertFalse(Strings::validateLength($origin, 0, 17));
        self::assertFalse(Strings::validateLength($origin, 17, 17));
        self::assertFalse(Strings::validateLength($origin, 19, 30));
        self::assertFalse(Strings::validateLength($origin, 19, 19));
    }

} //class
