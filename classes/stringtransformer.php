<?php namespace ironwoods\tools\strings\classes;

/**
 * @file: stringtransformer.php
 * @info: content several methods to change "Strings"
 *
 * @author: Moisés Alcocer
 * 2018, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.strings.classes
 * @version 0.0.13 (added)
 */

final class StringTransformer
{

    /******************************************************************/
    /*** Properties declaration ***************************************/


    /******************************************************************/
    /*** Methods declaration  *****************************************/

    /*** Public Methods ***********************************************/

        /**
         * Replaces slashes
         *
         * @param  string       $path
         * @return string
         */
        public static function replaceSlashes(string $path): string
        {
            return str_replace( '\\', '/', $path );
        }

        /**
         * Replaces special chars
         *
         * @param  string       $str
         * @return string
         */
        public static function replaceSpecialChars(string $str): string
        {
            if (StringChecks::hasSpecialChars($str)) {

                $str = str_replace(
                    [ 'á','é','í','ó','ú','Á','É','Í','Ó','Ú','ü','Ü','ñ','Ñ' ],
                    [ 'a','e','i','o','u','A','E','I','O','U','u','U','n','N' ],
                    $str
                );
            }

            return $str;
        }


    /*** Private Methods **********************************************/


} // class
