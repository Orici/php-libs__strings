<?php namespace ironwoods\tools\strings\classes;

/**
 * @file: stringtools.php
 * @info: content several generic methods to handle "Strings"
 *
 * @author: Moisés Alcocer
 * 2018, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.strings.classes
 * @version 0.0.01 (added)
 */

final class StringTools
{

    /******************************************************************/
    /*** Properties declaration ***************************************/


    /******************************************************************/
    /*** Methods declaration  *****************************************/

    /*** Public Methods ***********************************************/

        /**
         * Cuts a multi-row string into rows
         *
         * Method based in response from:
         *     https://es.stackoverflow.com/questions/128451 (user: Marcos)
         *
         * @param  string       $str
         * @return array
         */
        public static function getRows(string $str): array
        {
            return array_map('trim', preg_split('/\R/', $str));
        }

        /**
         * Searchs for special chars
         *
         * Si hay car. especiales en el string, pueden requerir usar
         * "utf8_decode()" para ser encontrados
         *
         * @param  string       $str
         * @return boolean
         */
        public static function hasSpecialChars(string $str): bool
        {
            $special_chars
                = [ 'á','é','í','ó','ú','Á','É','Í','Ó','Ú','ü','Ü','ñ','Ñ' ];

            foreach ($special_chars as $char) {

                if (self::isIn($str, $char)
                    || self::isIn($str, utf8_decode($char))) {
                    return true;
                }
            }


            return false;
        }

        /**
         * Searchs for a string content
         *
         * @param  string       $str
         * @param  string       $needle
         * @return boolean
         */
        public static function isIn(string $str, string $needle): bool
        {
            return (strpos($str, $needle) || strpos($str, $needle) === 0);
        }

} //class
