<?php namespace ironwoods\tools\strings\classes;

/**
 * @file: stringchecks.php
 * @info: content several methods to search in "Strings"
 *
 * @author: Moisés Alcocer
 * 2018, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.strings.classes
 * @version 0.0.14 (added)
 */

final class StringChecks
{

    /******************************************************************/
    /*** Properties declaration ***************************************/


    /******************************************************************/
    /*** Methods declaration  *****************************************/

    /*** Public Methods ***********************************************/

        /**
         * Searchs for special chars
         *
         * Si hay car. especiales en el string, pueden requerir usar
         * "utf8_decode()" para ser encontrados
         *
         * @param  string       $str
         * @return boolean
         */
        public static function hasSpecialChars(string $str): bool
        {
            $special_chars
                = [ 'á','é','í','ó','ú','Á','É','Í','Ó','Ú','ü','Ü','ñ','Ñ' ];

            foreach ($special_chars as $char) {

                if (self::isIn($str, $char)
                    || self::isIn($str, utf8_decode($char))) {
                    return true;
                }
            }


            return false;
        }

        /**
         * Searchs for a string content
         *
         * @param  string       $str
         * @param  string       $needle
         * @return boolean
         */
        public static function isIn(string $str, string $needle): bool
        {
            return (strpos($str, $needle) || strpos($str, $needle) === 0);
        }

        /**
         * Validates the string chars
         *
         * @param  string $str
         * @param  string $validChars
         * @return boolean
         */
        public static function validateChars(
            string $str,
            string $validChars
        ): bool
        {
            $len = mb_strlen($str);

            for ($i=0; $i<$len; $i++) {
                $char = mb_substr($str, $i, $i+1, "UTF-8");
                if ( ! strpbrk($char, $validChars)) {
                    return false;
                }
            }

            return true;
        }

        /**
         * Validates the string length
         *
         * @param  string $str
         * @param  string $minLength
         * @param  string $maxLength
         * @return boolean
         */
        public static function validateLength(
            string $str,
            int $minLength,
            int $maxLength
        ): bool
        {
            $len = mb_strlen($str);

            return (($len >= $minLength) && ($len <= $maxLength));
        }

} //class
