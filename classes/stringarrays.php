<?php namespace ironwoods\tools\strings\classes;

/**
 * @file: stringarrays.php
 * @info: content methods to handle arrays of "Strings"
 *
 * @author: Moisés Alcocer
 * 2018, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.strings.classes
 * @version 0.0.11 (added)
 */

final class StringArrays
{

    /******************************************************************/
    /*** Properties declaration ***************************************/


    /******************************************************************/
    /*** Methods declaration  *****************************************/

    /*** Public Methods ***********************************************/

        /**
         * Searchs a string inside the array
         *
         * @param  array        $strs
         * @param  string       $needle
         * @return boolean
         */
        public static function content(array $strs, string $needle): bool
        {
            if (! $strs) {
                return [];
            }

            foreach ($strs as $i => $str) {
                if (StringTools::isIn(utf8_encode($str), $needle)) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Gets the strings with the fragment inside
         *
         * @param  array        $strs
         * @param  string       $needle
         * @return array
         */
        public static function getWith(array $strs, string $needle): array
        {
            if (! $strs || $needle === '') {
                return [];
            }

            //Search for coincidences and put they into $result
            $result = array();
            foreach ($strs as $i => $str) {

                // HACK: traces
                // echo('Path: ' . $str . '<br>');
                // echo('Path: ' . utf8_encode($str) . '<br>');
                // echo('Path: ' . utf8_decode($str) . '<br>');

                if (StringTools::isIn(utf8_encode($str), $needle)
                    || StringTools::isIn($str, $needle)) {
                    $result[] = $str;
                }
            }

            return $result;
        }

} //class
