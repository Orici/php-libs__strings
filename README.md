PHP - Strings (Version: 0.0.16)
========================================================================
This library recopile several useful String methods used in other
projects

    Moisés Alcocer
    2018, <contacto@ironwoods.es>
    https://www.ironwoods.es
========================================================================

    How to use
    ********************************************************************

        1. Create the directory "ironwoods" inside your's "vendors"
    directory
        2. Clone this library inside.
        3. Rename to "strings" (if neccesary)
        4. Load the main file:
            require $vendors . '/ironwoods/strings/strings.php';
        Note: Alternatively to last step you can use a class in
    concrete, for example:
            require $vendors . '/ironwoods/strings/classes/stringtools.php';
        5. Declare the class namespace:
            use ironwoods\tools\strings\Strings as Strings;
        or
            use ironwoods\tools\strings\classes\StringTools as StringTools;
        to use directly the class "StringTools".

        Warning: if you "require" the main facade class musn't "require"
    neither specific class


    Available methods (in the facade class)
    ********************************************************************

          - Strings::getRows()         -> Cuts a multi-row string into
        rows
          - Strings::getWith()         -> Gets the strings that contain
        a fragment
          - Strings::hasSpecialChars() -> Searchs for special chars
          - Strings::isIn()            -> Searchs for a string content
          - Strings::isInArray()       -> Searchs a string inside the
        array
          - Strings::replaceSlashes()  -> Replaces slashes ("\" to "/")
          - Strings::replaceSpecialChars() -> Replaces special chars,
        for example "á" to "a"


    Testing
    ********************************************************************

        Some unit tests, implemented with PHPUnit, are available in
    "tests/StringsTests.php".
